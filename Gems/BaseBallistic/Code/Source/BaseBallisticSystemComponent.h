#pragma once

#include <AzCore/Component/Component.h>

#include <BaseBallistic/BaseBallisticBus.h>

namespace BaseBallistic
{
    class BaseBallisticSystemComponent
        : public AZ::Component
        , protected BaseBallisticRequestBus::Handler
    {
    public:
        AZ_COMPONENT(BaseBallisticSystemComponent, "{0610F554-E8F7-45F8-BDC2-63398AFFF510}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // BaseBallisticRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
