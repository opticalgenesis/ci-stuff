
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <BaseBossSystemComponent.h>

namespace BaseBoss
{
    class BaseBossModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(BaseBossModule, "{23B1CA6A-418F-4FAF-853E-DA6A9A513A73}", AZ::Module);
        AZ_CLASS_ALLOCATOR(BaseBossModule, AZ::SystemAllocator, 0);

        BaseBossModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                BaseBossSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<BaseBossSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(BaseBoss_98825f21467d41339a1963d0bdb7b2a8, BaseBoss::BaseBossModule)
