
#include <TestComponentSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace TestComponent
{
    void TestComponentSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
			serialize->Class<TestComponentSystemComponent, AZ::Component>()
				->Version(1)
				->Field("Example property", &TestComponentSystemComponent::m_privateProperty);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
				ec->Class<TestComponentSystemComponent>("TestComponent", "[Description of functionality provided by this System Component]")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
						->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
					->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->DataElement(AZ::Edit::UIHandlers::Default, &TestComponentSystemComponent::m_privateProperty,
						"SomeProperty", "Example property");
            }
        }
    } 

    void TestComponentSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("TestComponentService"));
    }

    void TestComponentSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("TestComponentService"));
    }

    void TestComponentSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void TestComponentSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void TestComponentSystemComponent::Init()
    {
    }

    void TestComponentSystemComponent::Activate()
    {
        TestComponentRequestBus::Handler::BusConnect();
    }

    void TestComponentSystemComponent::Deactivate()
    {
        TestComponentRequestBus::Handler::BusDisconnect();
    }
}
