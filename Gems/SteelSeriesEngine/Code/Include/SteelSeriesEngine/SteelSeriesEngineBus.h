#pragma once

#include <AzCore/EBus/EBus.h>
#include <string.h>

namespace SteelSeriesEngine
{
    class SteelSeriesEngineRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////

		struct GameRegistrationData 
		{
			std::string game;
			std::string gameDisplayName;
			std::string developer;
		};

		struct EventRegistrationData
		{
			std::string game;
			// Event is reserved keyword in C++
			std::string e;
			int minValue = 0;
			int maxValue = 100;
			int iconId = 0;
			bool valueOptional = false;
		};

		struct Data
		{
			// Int recommended for optimal experience
			int value;
		};

		struct GameEvent
		{
			std::string game;
			std::string e;
			Data data;
		};

		/*
			TODO
			STRUCT FOR ADVANCED DATA
		*/

        // Put your public methods here
		virtual const char* GetEngineAddress() = 0;
		virtual void SendHeartbeat(std::string) = 0;
		virtual void RegisterGame(GameRegistrationData) = 0;
		virtual void RegisterEvent(EventRegistrationData) = 0;
		virtual void SendGameEvent(GameEvent) = 0;
    };
    using SteelSeriesEngineRequestBus = AZ::EBus<SteelSeriesEngineRequests>;
} // namespace SteelSeriesEngine
