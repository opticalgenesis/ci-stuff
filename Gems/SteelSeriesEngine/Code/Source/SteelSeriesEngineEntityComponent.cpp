#include "SteelSeriesEngineEntityComponent.h"

#include <string>

#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

using namespace SteelSeriesEngine;

void SteelSeriesEngineEntityComponent::Reflect(AZ::ReflectContext* ctx)
{
	AZ::SerializeContext* s = azrtti_cast<AZ::SerializeContext*>(ctx);
	if (!s) return;

	s->Class<SteelSeriesEngineEntityComponent, Component>()
		->Version(0)
		;

	AZ::EditContext* e = s->GetEditContext();
	if (!e) return;

	e->Class<SteelSeriesEngineEntityComponent>("SteelSeries Engine", "Adds SSE3 functionality")
		->ClassElement(AZ::Edit::ClassElements::EditorData, "")
		->Attribute(AZ::Edit::Attributes::Category, "SteelSeries")
		->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
		;
}

void SteelSeriesEngineEntityComponent::Init()
{
}

void SteelSeriesEngineEntityComponent::Activate() 
{
	const char* b;
	SteelSeriesEngineRequestBus::BroadcastResult(b, 
		&SteelSeriesEngineRequestBus::Events::GetEngineAddress
	);

	SteelSeriesEngineRequestBus::Handler::GameRegistrationData d;
	d.developer = "lowpolybutt";
	d.game = "PP";
	d.gameDisplayName = "Project Penis";

	SteelSeriesEngineRequestBus::Broadcast(
		&SteelSeriesEngineRequestBus::Events::RegisterGame, d);
}
void SteelSeriesEngineEntityComponent::Deactivate() {}