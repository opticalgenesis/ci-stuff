#pragma once

#include <AzCore/Component/Component.h>

#include <SteelSeriesEngine/SteelSeriesEngineBus.h>

namespace SteelSeriesEngine
{
	class SteelSeriesEngineEntityComponent
		: public AZ::Component
	{
	public:
		AZ_COMPONENT(SteelSeriesEngineEntityComponent, "{0E8075B7-3FA9-4318-976C-D2986DBC3FB1}");

		static void Reflect(AZ::ReflectContext*);

	protected:
		void Init() override;
		void Activate() override;
		void Deactivate() override;
	};
}