
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TestGemSystemComponent.h>

namespace TestGem
{
    class TestGemModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TestGemModule, "{0619F649-CDF7-4D92-89F4-6CA64C40837D}", AZ::Module);
        AZ_CLASS_ALLOCATOR(TestGemModule, AZ::SystemAllocator, 0);

        TestGemModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TestGemSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<TestGemSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TestGem_109d5382d2734477914017910252f755, TestGem::TestGemModule)
