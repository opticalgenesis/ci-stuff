#pragma once

#include <AzCore/Component/Component.h>

#include <TestGem/TestGemBus.h>

namespace TestGem
{
    class TestGemSystemComponent
        : public AZ::Component
        , protected TestGemRequestBus::Handler
    {
    public:
        AZ_COMPONENT(TestGemSystemComponent, "{0B8DBBF3-E045-485B-929C-93B424007BE4}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // TestGemRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
