#pragma once

#include <AzCore/EBus/EBus.h>

namespace Async
{
    class AsyncRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////

        // Put your public methods here
		virtual void PrintEbusMessage() {}
    };
    using AsyncRequestBus = AZ::EBus<AsyncRequests>;
} // namespace Async
