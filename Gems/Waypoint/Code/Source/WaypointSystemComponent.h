#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/TickBus.h>
#include <AzCore/Component/TransformBus.h>

#include <AzFramework/Physics/PhysicsSystemComponentBus.h>

#include <Waypoint/WaypointBus.h>
#include <TDMinionBase/TDMinionBaseBus.h>

namespace Waypoint
{
    class WaypointSystemComponent
        : public AZ::Component
		, public AZ::TickBus::Handler
        , protected WaypointRequestBus::Handler
    {
    public:
        AZ_COMPONENT(WaypointSystemComponent, "{CEDB0D43-03CC-42FA-8921-ADC5BADFEF41}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // WaypointRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////////////
		// AZ::TickBus interface implementation
		void OnTick(float, AZ::ScriptTimePoint) override;

		AZ::Vector3 m_positionToCast;
		AZ::Vector3 m_directionToCast;
		AZ::Vector3 m_directionToDirect;

	private:
		AZ::Vector3 org;
		AZStd::vector<AZ::EntityId> idsToIgnore = AZStd::vector<AZ::EntityId>();
    };
}
