#pragma once

#include <AzCore/Component/Component.h>

#include <Contaminants/ContaminantsBus.h>

namespace Contaminants
{
    class ContaminantsSystemComponent
        : public AZ::Component
        , protected ContaminantsRequestBus::Handler
    {
    public:
        AZ_COMPONENT(ContaminantsSystemComponent, "{B3C6FB25-5D60-4525-AE32-170DD8FA5773}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // ContaminantsRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
