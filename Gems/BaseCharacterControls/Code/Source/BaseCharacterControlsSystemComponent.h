#pragma once

#include <AzCore/Component/Component.h>

#include <BaseCharacterControls/BaseCharacterControlsBus.h>

namespace BaseCharacterControls
{
    class BaseCharacterControlsSystemComponent
        : public AZ::Component
        , protected BaseCharacterControlsRequestBus::Handler
    {
    public:
        AZ_COMPONENT(BaseCharacterControlsSystemComponent, "{11746AA4-B5B6-4A04-B0C1-1BEB8B4FB376}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // BaseCharacterControlsRequestBus interface implementation
		void MoveForward(ActionState) override;
		void MoveBackward(ActionState) override;
		void MoveLeft(ActionState) override;
		void MoveRight(ActionState) override;
		void LookVertical(float) override;
		void LookHorizontal(float) override;

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
