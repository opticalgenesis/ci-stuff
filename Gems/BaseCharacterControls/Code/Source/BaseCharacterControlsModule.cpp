
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <BaseCharacterControlsSystemComponent.h>
#include <InputCaptureComponent.h>

namespace BaseCharacterControls
{
    class BaseCharacterControlsModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(BaseCharacterControlsModule, "{647C4DC2-F58D-462A-8AF6-395B09CCD4CA}", AZ::Module);
        AZ_CLASS_ALLOCATOR(BaseCharacterControlsModule, AZ::SystemAllocator, 0);

        BaseCharacterControlsModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                BaseCharacterControlsSystemComponent::CreateDescriptor(),
				InputCaptureComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<BaseCharacterControlsSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(BaseCharacterControls_b9c9e8953be3460d9a1aeca426435b4b, BaseCharacterControls::BaseCharacterControlsModule)
