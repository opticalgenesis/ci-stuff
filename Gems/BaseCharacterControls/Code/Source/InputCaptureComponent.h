#pragma once

#include <AzCore/Component/Component.h>
#include <AzFramework/Input/Events/InputChannelEventListener.h>

namespace BaseCharacterControls
{
	class InputCaptureComponent
		: public AZ::Component
		, public AzFramework::InputChannelEventListener
	{
	public:
		AZ_COMPONENT(InputCaptureComponent, "{73FF081A-989E-4909-AA58-487F168BB593}");

		static void Reflect(AZ::ReflectContext*);

		void Activate() override;
		void Deactivate() override;

	protected:
		bool OnInputChannelEventFiltered(const AzFramework::InputChannel&) override;
		bool OnKeyboardEvent(const AzFramework::InputChannel&);

	private:
		void MoveForward(bool);
		void MoveBackward(bool);
		void StrafeLeft(bool);
		void StrafeRight(bool);

		void SetEmotionParam(const char*, float);

		bool m_isMovingForward = false;
		bool m_isMovingBackward = false;
	};
}