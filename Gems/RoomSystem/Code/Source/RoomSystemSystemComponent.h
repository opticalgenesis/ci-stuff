#pragma once

#include <AzCore/Component/Component.h>

#include <RoomSystem/RoomSystemBus.h>

namespace RoomSystem
{
    class RoomSystemSystemComponent
        : public AZ::Component
        , protected RoomSystemRequestBus::Handler
    {
    public:
        AZ_COMPONENT(RoomSystemSystemComponent, "{8BAE59EA-FCD2-4486-B551-7B3E93CFB2C1}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

		struct SGas
		{
			AZStd::string m_gasName;
			float m_atmosComp;
		};

		struct SClimate
		{
			float m_climateTemperature;
			float m_climateHumidity;
		};

		struct SAtmoComp
		{
			SGas m_oxyComp;
			SGas m_nitroComp;
			SGas m_carbonDioxComp;
			AZStd::vector<SGas> m_otherGasses;
		};


    protected:
        ////////////////////////////////////////////////////////////////////////
        // RoomSystemRequestBus interface implementation
		SClimate m_climateComp;
		SAtmoComp m_atmoComp;

		float m_oxyComp;
		float m_nitroComp;
		float m_carbonDioxComp;

		float m_climateTemperature;
		float m_climateHumidity;

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////

	private:
		void UpdateClimateOxy(float);
    };
}
