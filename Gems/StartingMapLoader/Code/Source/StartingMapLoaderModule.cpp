
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <StartingMapLoaderSystemComponent.h>

namespace StartingMapLoader
{
    class StartingMapLoaderModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(StartingMapLoaderModule, "{FC134906-DFCC-4ADA-873C-BC70C2BDA97F}", AZ::Module);
        AZ_CLASS_ALLOCATOR(StartingMapLoaderModule, AZ::SystemAllocator, 0);

        StartingMapLoaderModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                StartingMapLoaderSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<StartingMapLoaderSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(StartingMapLoader_e040b1f7d26c490c9d66e13f4984b7a9, StartingMapLoader::StartingMapLoaderModule)
