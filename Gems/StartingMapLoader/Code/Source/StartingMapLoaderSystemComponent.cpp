
#include <StartingMapLoaderSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

#include <platform.h>
#include <platform_impl.h>
#include <CrySystemBus.h>
#include <ISystem.h>

namespace StartingMapLoader
{
    void StartingMapLoaderSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
			serialize->Class<StartingMapLoaderSystemComponent, AZ::Component>()
				->Version(1)
				->Field("Client Map", &StartingMapLoaderSystemComponent::m_clientMap)
				->Field("Server Map", &StartingMapLoaderSystemComponent::m_serverMap)
				;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<StartingMapLoaderSystemComponent>("StartingMapLoader", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->DataElement(nullptr, &StartingMapLoaderSystemComponent::m_clientMap, 
						"Client map", "The map to load on the client")
					->DataElement(nullptr, &StartingMapLoaderSystemComponent::m_serverMap,
						"Server map", "The map to load on the server")
                    ;
            }
        }
    }

    void StartingMapLoaderSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("StartingMapLoaderService"));
    }

    void StartingMapLoaderSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("StartingMapLoaderService"));
    }

    void StartingMapLoaderSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void StartingMapLoaderSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void StartingMapLoaderSystemComponent::Init()
    {
    }

    void StartingMapLoaderSystemComponent::Activate()
    {
        StartingMapLoaderRequestBus::Handler::BusConnect();
		AZ::SystemTickBus::Handler::BusConnect();
    }

    void StartingMapLoaderSystemComponent::Deactivate()
    {
        StartingMapLoaderRequestBus::Handler::BusDisconnect();
		AZ::SystemTickBus::Handler::BusDisconnect();
    }

	void StartingMapLoaderSystemComponent::OnSystemTick()
	{
		ISystem* sys = nullptr;

		CrySystemRequestBus::BroadcastResult(sys, &CrySystemRequestBus::Events::GetCrySystem);

		if (sys && sys->GetGlobalEnvironment()->IsEditor())
		{
			AZ::SystemTickBus::Handler::BusDisconnect();
			return;
		}

		AZStd::string mapCommand("map ");
#if defined(DEDICATED_SERVER)
		AzFramework::ConsoleRequestBus::Broadcast(
			&AzFramework::ConsoleRequests::ExecuteConsoleCommand, "mphost"
		);
		mapCommand += m_serverMap;
#else
		mapCommand += m_clientMap;
#endif

		AzFramework::ConsoleRequestBus::Broadcast(
			&AzFramework::ConsoleRequests::ExecuteConsoleCommand, mapCommand.c_str()
		);
		AZ::SystemTickBus::Handler::BusDisconnect();
	}
}
