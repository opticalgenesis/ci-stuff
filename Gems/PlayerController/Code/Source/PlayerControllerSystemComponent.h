#pragma once

#include <AzCore/Component/Component.h>

#include <PlayerController/PlayerControllerBus.h>

namespace PlayerController
{
    class PlayerControllerSystemComponent
        : public AZ::Component
        , protected PlayerControllerRequestBus::Handler
    {
    public:
        AZ_COMPONENT(PlayerControllerSystemComponent, "{708C8AE3-DC91-4DD8-A42F-1EDD6FAFE833}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // PlayerControllerRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
