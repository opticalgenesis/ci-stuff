#pragma once

#include <AzCore/Component/EntityId.h>
#include <AzCore/EBus/EBus.h>
#include <AzCore/Math/Vector3.h>

namespace Projector
{
	class ProjectionSpawnerRequests
		: public AZ::EBusTraits
	{
	public:
		static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
		static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;

		/*using BusIdType = AZ::EntityId;*/

		virtual void SpawnProjection(AZ::Vector3) = 0;
	};

	using ProjectionSpawnerRequestsBus = AZ::EBus<ProjectionSpawnerRequests>;
}