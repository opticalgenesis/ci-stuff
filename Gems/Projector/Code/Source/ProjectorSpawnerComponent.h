#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>

#include <AzFramework/Input/Buses/Requests/InputSystemCursorRequestBus.h>

#include <LmbrCentral/Scripting/SpawnerComponentBus.h>

#include <LyShine/Bus/UiCursorBus.h>

#include <Projector/ProjectionSpawnerBus.h>
#include <Projector/ProjectorBus.h>

namespace Projector
{
	class ProjectorSpawnerComponent
		: public AZ::Component
		, protected ProjectionSpawnerRequestsBus::Handler
	{
	public:
		AZ_COMPONENT(ProjectorSpawnerComponent, "{AFFBEECA-03F9-47CC-B76C-D024C6A47C0E}", Component);

		static void Reflect(AZ::ReflectContext*);

		void Init() override;
		void Activate() override;
		void Deactivate() override;

		void SpawnProjection(AZ::Vector3) override;
	};
}