#include "ProjectorSpawnerComponent.h"

namespace Projector
{
	void ProjectorSpawnerComponent::Reflect(AZ::ReflectContext* context)
	{
		AZ::SerializeContext* s = azrtti_cast<AZ::SerializeContext*>(context);
		if (!s) return;
		s->Class<ProjectorSpawnerComponent, AZ::Component>()
			->Version(0)
			;

		AZ::EditContext* e = s->GetEditContext();
		if (!e) return;
		e->Class<ProjectorSpawnerComponent>("ProjectionSpawner", "")
			->ClassElement(AZ::Edit::ClassElements::EditorData, "")
			->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
			->Attribute(AZ::Edit::Attributes::AutoExpand, true)
			->Attribute(AZ::Edit::Attributes::Category, "TowerDefence")
			;
	}

	void ProjectorSpawnerComponent::Init()
	{}

	void ProjectorSpawnerComponent::Activate()
	{
		ProjectionSpawnerRequestsBus::Handler::BusConnect();
	}

	void ProjectorSpawnerComponent::Deactivate()
	{
		ProjectionSpawnerRequestsBus::Handler::BusDisconnect();
	}

	void ProjectorSpawnerComponent::SpawnProjection(AZ::Vector3 v)
	{
		UiCursorBus::Broadcast(&UiCursorBus::Events::DecrementVisibleCounter);
		AzFramework::InputSystemCursorRequestBus::Broadcast(
			&AzFramework::InputSystemCursorRequestBus::Events::SetSystemCursorPositionNormalized,
			AZ::Vector2(.5f, .5f)
		);
		LmbrCentral::SpawnerComponentRequestBus::Event(GetEntityId(),
			&LmbrCentral::SpawnerComponentRequestBus::Events::Spawn);
		Projector::ProjectorRequestBus::Broadcast(&Projector::ProjectorRequestBus::Events::SetEntitySpawned, true);
	}
}