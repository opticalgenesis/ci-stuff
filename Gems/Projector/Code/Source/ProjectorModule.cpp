
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <ProjectorSystemComponent.h>
#include <ProjectorSpawnerComponent.h>

namespace Projector
{
    class ProjectorModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(ProjectorModule, "{53AE228B-1C21-4146-8623-19829D3B501A}", AZ::Module);
        AZ_CLASS_ALLOCATOR(ProjectorModule, AZ::SystemAllocator, 0);

        ProjectorModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                ProjectorSystemComponent::CreateDescriptor(),
				ProjectorSpawnerComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(Projector_18195a909ac04fffb3ffaa4a4afff621, Projector::ProjectorModule)
