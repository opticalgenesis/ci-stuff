
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <NetworkedPlayerSystemComponent.h>

namespace NetworkedPlayer
{
    class NetworkedPlayerModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(NetworkedPlayerModule, "{2349B9CB-8F3F-4B35-A0A4-95A6CF52F3C1}", AZ::Module);
        AZ_CLASS_ALLOCATOR(NetworkedPlayerModule, AZ::SystemAllocator, 0);

        NetworkedPlayerModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                NetworkedPlayerSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<NetworkedPlayerSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(NetworkedPlayer_ef76989b47034ce4a0639fc662de6b01, NetworkedPlayer::NetworkedPlayerModule)
