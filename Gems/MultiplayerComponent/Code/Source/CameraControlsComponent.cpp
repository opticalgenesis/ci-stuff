#include "CameraControlsComponent.h"

#include <AzCore/Component/Component.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Math/Vector3.h>
#include <AzCore/Component/TransformBus.h>

using namespace MultiplayerComponent;

void CameraControlsComponent::Activate()
{
	PlayerControlsRequestBus::Handler::BusConnect(GetEntityId());
}

void CameraControlsComponent::Deactivate()
{
	PlayerControlsRequestBus::Handler::BusDisconnect();
}

void CameraControlsComponent::Reflect(AZ::ReflectContext* ctx)
{
	AZ::SerializeContext* s_ctx = azrtti_cast<AZ::SerializeContext*>(ctx);

	if (!s_ctx) return;

	s_ctx->Class<CameraControlsComponent, Component>()
		->Field("Look Up Speed", &CameraControlsComponent::fLookVerticalSpeed)
		->Version(0)
		;

	AZ::EditContext* e_ctx = s_ctx->GetEditContext();

	if (!e_ctx) return;

	e_ctx->Class<CameraControlsComponent>("Camera Control Component", "Controls the camera")
		->ClassElement(AZ::Edit::ClassElements::EditorData, "")
		->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
		->Attribute(AZ::Edit::Attributes::Category, "Multiplayer Character")
		->DataElement(nullptr, &CameraControlsComponent::fLookVerticalSpeed,
			"Look Up Speed", "How fast the camera looks up")
		;
}

void CameraControlsComponent::LookVertical(float amt)
{
	fRotY = amt * fLookVerticalSpeed;
	SetRotation();
}

void CameraControlsComponent::SetRotation()
{
	AZ::EntityId parent;

	AZ::TransformBus::EventResult(parent, GetEntityId() ,&AZ::TransformBus::Events::GetParentId);

	AZ::TransformBus::Event(parent, &AZ::TransformBus::Events::SetLocalRotationQuaternion,
		AZ::Quaternion::CreateRotationX(fRotY));
}