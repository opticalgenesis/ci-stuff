#include "InputCaptureComponent.h"

#include <MultiplayerComponent/PlayerControlsRequestBus.h>

#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/SerializeContext.h>

#include <AzFramework/Input/Devices/Keyboard/InputDeviceKeyboard.h>
#include <AzFramework/Input/Devices/Mouse/InputDeviceMouse.h>

using namespace MultiplayerComponent;

void InputCaptureComponent::Activate()
{
	AzFramework::InputChannelEventListener::Connect();
}

void InputCaptureComponent::Deactivate()
{
	AzFramework::InputChannelEventListener::Disconnect();
}

void InputCaptureComponent::Reflect(AZ::ReflectContext* ctx)
{
	AZ::SerializeContext* s_ctx = azrtti_cast<AZ::SerializeContext*>(ctx);
	if (!s_ctx) return;

	s_ctx->Class<InputCaptureComponent, Component>()
		->Version(0)
		;

	AZ::EditContext* e_ctx = s_ctx->GetEditContext();
	if (!e_ctx) return;

	e_ctx->Class<InputCaptureComponent>("Player Input Capture", "[Captures input in C++]")
		->ClassElement(AZ::Edit::ClassElements::EditorData, "")
		->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
		->Attribute(AZ::Edit::Attributes::Category, "Multiplayer Component")
		;
}

bool InputCaptureComponent::OnInputChannelEventFiltered(
	const AzFramework::InputChannel& channel
)
{
	const AzFramework::InputDeviceId devId = channel.GetInputDevice().GetInputDeviceId();

	if (devId == AzFramework::InputDeviceKeyboard::Id)
		return OnKeyboardEvent(channel);

	if (devId == AzFramework::InputDeviceMouse::Id)
		return OnMouseEvent(channel);

	return false;
}

bool InputCaptureComponent::OnKeyboardEvent(const AzFramework::InputChannel& kb)
{
	const AzFramework::InputChannelId inputType = kb.GetInputChannelId();

	if (inputType == AzFramework::InputDeviceKeyboard::Key::EditSpace)
	{
		const bool pressed = !!kb.GetValue();
		ShootProjectile(pressed);
		return true;
	}

	if (inputType == AzFramework::InputDeviceKeyboard::Key::AlphanumericW)
	{
		const bool pressed = !!kb.GetValue();
		CheckAndUpdateForward(pressed);
		return true;
	}

	if (inputType == AzFramework::InputDeviceKeyboard::Key::AlphanumericS)
	{
		const bool pressed = !!kb.GetValue();
		CheckAndUpdateBackward(pressed);
		return true;
	}

	if (inputType == AzFramework::InputDeviceKeyboard::Key::AlphanumericA)
	{
		const bool pressed = !!kb.GetValue();
		CheckAndUpdateStrafeLeft(pressed);
		return true;
	}

	if (inputType == AzFramework::InputDeviceKeyboard::Key::AlphanumericD)
	{
		const bool pressed = !!kb.GetValue();
		CheckAndUpdateStrafeRight(pressed);
		return true;
	}

	return false;
}

void InputCaptureComponent::CheckAndUpdateForward(bool p)
{
	if (bIsForwardPressed == p) return;

	PlayerControlsRequestBus::Broadcast(
		&PlayerControlsRequestBus::Events::MoveForward,
		p ? ActionState::Started : ActionState::Stopped
	);

	bIsForwardPressed = p;

	AZ_Printf("ICC", "%s", "Forward is pressed");
}

void InputCaptureComponent::CheckAndUpdateBackward(bool p)
{
	if (bIsBackwardPressed == p) return;

	PlayerControlsRequestBus::Broadcast(
		&PlayerControlsRequestBus::Events::MoveBackward,
		p ? ActionState::Started : ActionState::Stopped
	);

	bIsBackwardPressed = p;
}

void InputCaptureComponent::CheckAndUpdateStrafeLeft(bool p)
{
	if (bIsStrafingLeft == p) return;

	PlayerControlsRequestBus::Broadcast(
		&PlayerControlsRequestBus::Events::StrafeLeft,
		p ? ActionState::Started : ActionState::Stopped
	);

	bIsStrafingLeft = p;
}

void InputCaptureComponent::CheckAndUpdateStrafeRight(bool p)
{
	if (bIsStrafingRight == p) return;

	PlayerControlsRequestBus::Broadcast(
		&PlayerControlsRequestBus::Events::StrafeRight,
		p ? ActionState::Started : ActionState::Stopped
	);

	bIsStrafingRight = p;
}

void InputCaptureComponent::ShootProjectile(bool b)
{
	AZ_Printf("ICC", "%s", "Shoot");
	if (bIsShooting == b) return;

	PlayerControlsRequestBus::Broadcast(
		&PlayerControlsRequestBus::Events::Shoot,
		b ? ActionState::Started : ActionState::Stopped
	);

	bIsShooting = b;
}

bool InputCaptureComponent::OnMouseEvent(const AzFramework::InputChannel& m)
{
	//AZ_Printf("OME", "%s", "Mouse event detected but ignored");
	//AZ_UNUSED(m);
	const AzFramework::InputChannelId inputChannelId = m.GetInputChannelId();

	if (inputChannelId == AzFramework::InputDeviceMouse::Button::Left
		|| inputChannelId == AzFramework::InputDeviceMouse::Button::Right)
	{
		return false;
	}

	if (inputChannelId == AzFramework::InputDeviceMouse::SystemCursorPosition)
	{
		const AzFramework::InputChannel::PositionData2D* pos = m.GetCustomData<AzFramework::InputChannel::PositionData2D>();
		if (!pos) return false;

		const AZ::Vector2& position = pos->m_normalizedPosition;

		static const AZ::Vector2 center = AZ::Vector2{ .5f,.5f };

		vMouseChangeAggregate += center - position;

		AzFramework::InputSystemCursorRequestBus::Broadcast(
			&AzFramework::InputSystemCursorRequestBus::Events
			::SetSystemCursorPositionNormalized, center
		);

		PlayerControlsRequestBus::Broadcast(
			&PlayerControlsRequestBus::Events::Turn,
			vMouseChangeAggregate.GetX()
		);

		PlayerControlsRequestBus::Broadcast(
			&PlayerControlsRequestBus::Events::LookVertical,
			vMouseChangeAggregate.GetY()
		);

		return true;
	}
	return false;
}