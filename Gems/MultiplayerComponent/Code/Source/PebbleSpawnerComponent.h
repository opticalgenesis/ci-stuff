#pragma once
#include <AzCore/Component/Component.h>
#include <AzFramework/Network/NetBindable.h>
#include <GridMate/Replica/ReplicaFunctions.h>
#include <GridMate/Serialize/MathMarshal.h>
#include <MultiplayerComponent/PebbleSpawnerBus.h>

namespace MultiplayerComponent
{
	class PebbleSpawnerComponent
		: public AZ::Component
		, public PebbleSpawnerComponentBus::Handler
		, public AzFramework::NetBindable
	{
	public:
		AZ_COMPONENT(PebbleSpawnerComponent, "{284BEB2D-D2EA-4D37-AA1E-8FFA5619928E}",
			AzFramework::NetBindable);

		class Chunk;

		bool OnPebbleSpawn(AZ::Transform, const GridMate::RpcContext&);

	protected:
		void Activate() override;
		void Deactivate() override;

		static void Reflect(AZ::ReflectContext*);

		void SpawnPebbleAt(const AZ::Transform&) override;

		GridMate::ReplicaChunkPtr GetNetworkBinding() override;

		void SetNetworkBinding(GridMate::ReplicaChunkPtr) override;
		void UnbindFromNetwork() override;

	private:
		GridMate::ReplicaChunkPtr gmChunk;
	};
}