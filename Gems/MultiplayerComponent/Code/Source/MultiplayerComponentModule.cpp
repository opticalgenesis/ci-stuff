
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <MultiplayerComponentSystemComponent.h>
#include <PlayerControlsComponent.h>
#include <CameraControlsComponent.h>
#include <InputCaptureComponent.h>
#include <TimedProjectileComponent.h>
#include <PebbleSpawnerComponent.h>

namespace MultiplayerComponent
{
    class MultiplayerComponentModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(MultiplayerComponentModule, "{88891305-24B2-420B-856C-AC28657328BE}", AZ::Module);
        AZ_CLASS_ALLOCATOR(MultiplayerComponentModule, AZ::SystemAllocator, 0);

        MultiplayerComponentModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                MultiplayerComponentSystemComponent::CreateDescriptor(),
				PlayerControlsComponent::CreateDescriptor(),
				CameraControlsComponent::CreateDescriptor(),
				InputCaptureComponent::CreateDescriptor(),
				TimedProjectileComponent::CreateDescriptor(),
				PebbleSpawnerComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<MultiplayerComponentSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(MultiplayerComponent_a272bcf7074b45f3957eed0da7ac9590, MultiplayerComponent::MultiplayerComponentModule)
