#include "TimedProjectileComponent.h"

#include <AzCore/Serialization/EditContext.h>
#include <AzFramework/Entity/GameEntityContextBus.h>
#include <AzFramework/Network/NetBindingHandlerBus.h>
#include <AzCore/Component/TransformBus.h>

using namespace MultiplayerComponent;

void TimedProjectileComponent::Activate()
{
	if (AzFramework::NetQuery::IsEntityAuthoritative(GetEntityId()))
	{
		AZ::TickBus::Handler::BusConnect();

		bool bIsPhysicsEnabled = false;
		Physics::RigidBodyRequestBus::EventResult(
			bIsPhysicsEnabled, GetEntityId(),
			&Physics::RigidBodyRequestBus::Events::IsPhysicsEnabled
		);

		if (bIsPhysicsEnabled)
		{
			SetInitialVelocity();
		}
		else
		{
			Physics::RigidBodyNotificationBus::Handler::BusConnect(GetEntityId());
		}
	}
}

void TimedProjectileComponent::Deactivate()
{
	AZ::TickBus::Handler::BusDisconnect();
}

void TimedProjectileComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& req)
{
	req.push_back(AZ_CRC("PhysXRigidBodyService"));
}

void TimedProjectileComponent::Reflect(AZ::ReflectContext* ctx)
{
	AZ::SerializeContext* s_ctx = azrtti_cast<AZ::SerializeContext*>(ctx);

	if (!s_ctx) return;

	s_ctx->Class<TimedProjectileComponent, Component>()
		->Field("Initial velocity", &TimedProjectileComponent::fVelocity)
		->Field("Max life", &TimedProjectileComponent::fMaxLifeTime)
		->Version(0)
		;

	AZ::EditContext* e_ctx = s_ctx->GetEditContext();

	if (!e_ctx) return;

	e_ctx->Class<TimedProjectileComponent>("Timed Projectile", "A projectile component with a limited lifespan")
		->ClassElement(AZ::Edit::ClassElements::EditorData, "")
		->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
		->Attribute(AZ::Edit::Attributes::Category, "Multiplayer Component")
		->DataElement(nullptr, &TimedProjectileComponent::fVelocity, "Projectile velocity",
			"The velocity of the projectile")
		->DataElement(nullptr, &TimedProjectileComponent::fMaxLifeTime, "Max lifetime",
			"The max lifetime of the projectil")
		;
}

void TimedProjectileComponent::OnTick(float dt, AZ::ScriptTimePoint pt)
{
	fLifetime += dt;

	if (fLifetime > fMaxLifeTime)
	{
		AZ::TickBus::Handler::BusDisconnect();
		AzFramework::GameEntityContextRequestBus::Broadcast(
			&AzFramework::GameEntityContextRequestBus::Events::
			DestroyGameEntity, GetEntityId()
		);
	}
}

void TimedProjectileComponent::OnPhysicsEnabled()
{
	Physics::RigidBodyNotificationBus::Handler::BusDisconnect();
	SetInitialVelocity();
}

void TimedProjectileComponent::OnPhysicsDisabled()
{
	AZ_UNUSED("");
}

void TimedProjectileComponent::SetInitialVelocity()
{
	AZ::Quaternion qRot;

	AZ::TransformBus::EventResult(qRot, GetEntityId(),
		&AZ::TransformBus::Events::GetWorldRotationQuaternion);

	AZ::Vector3 dir = qRot * AZ::Vector3::CreateAxisY(fVelocity);

	Physics::RigidBodyRequestBus::Event(GetEntityId(),
		&Physics::RigidBodyRequestBus::Events::SetLinearVelocity, dir);
}
