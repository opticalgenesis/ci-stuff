#pragma once

#include <AzCore/Component/Component.h>
#include <MultiplayerComponent/PlayerControlsRequestBus.h>
#include <AzCore/Component/TickBus.h>
#include <MultiplayerComponent/PebbleSpawnerBus.h>

namespace MultiplayerComponent
{
	class PlayerControlsComponent
		: public AZ::Component
		, public AZ::TickBus::Handler
		, public PlayerControlsRequestBus::Handler
	{
	public:
		AZ_COMPONENT(PlayerControlsComponent, "{8C817620-FB02-4F0F-8392-8BD3563E21A5}");

		void Activate() override;
		void Deactivate() override;

		static void Reflect(AZ::ReflectContext*);

	protected:
		void MoveForward(ActionState) override;
		void MoveBackward(ActionState) override;
		void StrafeLeft(ActionState) override;
		void StrafeRight(ActionState) override;

		void Turn(float) override;

		void Shoot(ActionState) override;

		void OnTick(float, AZ::ScriptTimePoint) override;

	private:
		bool bIsForward = false;
		bool bIsBackward = false;
		bool bIsLeft = false;
		bool bIsRight = false;

		float fSpeed = .1f;
		float fTurnSpeed = 3.f;
		float fGravity = -.981f;

		float fRotZ = .0f;

		void SetRotation();
	};
}