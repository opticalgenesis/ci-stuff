#pragma once

#include <AzCore/EBus/EBus.h>

namespace TDPlayerController
{
    class TDPlayerControllerRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////

        // Put your public methods here
		virtual void BeginPlace() = 0;
    };
    using TDPlayerControllerRequestBus = AZ::EBus<TDPlayerControllerRequests>;
} // namespace TDPlayerController
