#pragma once

#include <AzCore/Component/Component.h>

#include <AsyncSpawner/AsyncSpawnerBus.h>

namespace AsyncSpawner
{
    class AsyncSpawnerSystemComponent
        : public AZ::Component
        , protected AsyncSpawnerRequestBus::Handler
    {
    public:
        AZ_COMPONENT(AsyncSpawnerSystemComponent, "{6C8E9BA6-498E-421B-9D7A-47825F20A240}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // AsyncSpawnerRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
