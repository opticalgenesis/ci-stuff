#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/EntityId.h>

namespace Room
{
	class RoomContaminantRequests
		: public AZ::EBusTraits
	{
	public:
		static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
		static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;

		using BusIdType = AZ::EntityId;

		enum EInfectionType
		{
			I_TYPE_BACTERIA,
			I_TYPE_VIRUS,
			I_TYPE_FUNGAL,
			I_TYPE_PRION
		};

		enum EBacteriaType
		{
			GRAM_POSITIVE,
			GRAM_NEGATIVE
		};
	};

	using RoomContaminatRequestBus = AZ::EBus<RoomContaminantRequests>;
}