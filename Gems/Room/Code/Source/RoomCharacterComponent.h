#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/ComponentApplicationBus.h>
#include <AzCore/Component/Entity.h>
#include <AzCore/Component/TickBus.h>
#include <AzCore/Component/TransformBus.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/SerializeContext.h>

#include <AzFramework/Physics/CharacterSystemBus.h>
#include <AzFramework/Physics/PhysicsComponentBus.h>
#include <AzFramework/Physics/PhysicsSystemComponentBus.h>

#include <LyShine/Bus/UiTextBus.h>

#include <Source/RoomSystemComponent.h>
#include <Room/RoomBus.h>
#include <Room/RoomUiBus.h>

namespace Room
{
	class RoomCharacterComponent
		: public AZ::Component
		, public AZ::TickBus::Handler
	{
	public:
		AZ_COMPONENT(RoomCharacterComponent, "{F71682CF-E677-4A32-9708-B1D51931B1ED}");

		static void Reflect(AZ::ReflectContext*);

	protected:
		void Init() override;
		void Activate() override;
		void Deactivate() override;

		void OnTick(float, AZ::ScriptTimePoint) override;

		float m_health = 100.f;
		float m_minTemp = .0f;
		float m_maxTemp = 50.f;
		float m_internalTemp = 37.5f;
		const float m_baseInternalTemp = 37.5f;
		float m_heatEmergency = 41.f;
		float m_coldEmergency = 32.f;
		float m_tempDecayRate = 0.04;

	private:
		void FindRoom();
		void UpcayInternalTemperature();
		void DecayInternalTemperature();

		void ResetTemperature();

		bool m_resetLock = true;
		bool m_shouldChangeTemp = false;

		AZStd::thread m_decayThread;
		RoomRequests::SAtmo s;
		bool m_isDecaying = false;
	};
}