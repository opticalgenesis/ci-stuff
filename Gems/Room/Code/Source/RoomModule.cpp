
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <RoomSystemComponent.h>
#include <RoomCharacterComponent.h>
#include <RoomUiComponent.h>

namespace Room
{
    class RoomModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(RoomModule, "{817E1B0A-CB22-43F6-A0B7-9E2DF45D390F}", AZ::Module);
        AZ_CLASS_ALLOCATOR(RoomModule, AZ::SystemAllocator, 0);

        RoomModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                RoomSystemComponent::CreateDescriptor(),
				RoomCharacterComponent::CreateDescriptor(),
				RoomUiComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(Room_189d6a9b69ac4cc5a4fde2fe6310aa6b, Room::RoomModule)
