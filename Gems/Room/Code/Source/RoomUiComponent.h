#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/SerializeContext.h>

#include <LyShine/Bus/UiInteractableBus.h>
#include <LyShine/Bus/UiTextBus.h>
// Required for ^
#include <platform_impl.h>

#include <Room/RoomBus.h>
#include <Room/RoomUiBus.h>

namespace Room
{
	class RoomUiComponent
		: public AZ::Component
		, protected RoomUiRequestBus::Handler
	{
	public:
		AZ_COMPONENT(RoomUiComponent, "{16E02678-982C-47F1-A96F-1FB119C10E9B}");

		static void Reflect(AZ::ReflectContext*);

	protected:
		void Init() override {};
		void Activate() override;
		void Deactivate() override;

		void UpdateRoom(RoomRequests::SAtmo) override;
	};
}