#include "RoomUiComponent.h"

namespace Room
{
	void RoomUiComponent::Reflect(AZ::ReflectContext* c)
	{
		if (AZ::SerializeContext* s = azrtti_cast<AZ::SerializeContext*>(c))
		{
			s->Class<RoomUiComponent, AZ::Component>()
				->Version(0)
				;

			if (AZ::EditContext* e = s->GetEditContext())
			{
				e->Class<RoomUiComponent>("Room UI", "UI component for the Room System")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
					->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("UI"))
					;
			}
		}
	}

	void RoomUiComponent::Activate()
	{
		RoomUiRequestBus::Handler::BusConnect(GetEntityId());
	}

	void RoomUiComponent::Deactivate()
	{
		RoomUiRequestBus::Handler::BusDisconnect();
	}

	void RoomUiComponent::UpdateRoom(RoomRequests::SAtmo a)
	{
		AZStd::string f = AZStd::string::format("%s: %f", "Temperature", round(a.m_climate.m_temperature));
		UiTextBus::Event(GetEntityId(), &UiTextBus::Events::SetText, f);
	}
}