
#include <TDBaseProjectileSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace TDBaseProjectile
{
    void TDBaseProjectileSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<TDBaseProjectileSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<TDBaseProjectileSystemComponent>("TDBaseProjectile", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void TDBaseProjectileSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("TDBaseProjectileService"));
    }

    void TDBaseProjectileSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("TDBaseProjectileService"));
    }

    void TDBaseProjectileSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void TDBaseProjectileSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void TDBaseProjectileSystemComponent::Init()
    {
    }

    void TDBaseProjectileSystemComponent::Activate()
    {
        TDBaseProjectileRequestBus::Handler::BusConnect();
    }

    void TDBaseProjectileSystemComponent::Deactivate()
    {
        TDBaseProjectileRequestBus::Handler::BusDisconnect();
    }
}
