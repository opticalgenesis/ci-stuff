
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <CMovement.h>

#include <SimplePlayerControlsSystemComponent.h>

namespace SimplePlayerControls
{
    class SimplePlayerControlsModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(SimplePlayerControlsModule, "{D668271C-990B-4C71-BF6B-538A84A540A3}", AZ::Module);
        AZ_CLASS_ALLOCATOR(SimplePlayerControlsModule, AZ::SystemAllocator, 0);

        SimplePlayerControlsModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                SimplePlayerControlsSystemComponent::CreateDescriptor(),
				CMovement::CreateDescriptor()
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(SimplePlayerControls_d2ff18491af64d4c973e4d5e46d118a2, SimplePlayerControls::SimplePlayerControlsModule)
