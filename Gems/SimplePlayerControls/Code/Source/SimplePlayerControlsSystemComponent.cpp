
#include <SimplePlayerControlsSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace SimplePlayerControls
{
    void SimplePlayerControlsSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<SimplePlayerControlsSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<SimplePlayerControlsSystemComponent>("SimplePlayerControls", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void SimplePlayerControlsSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("SimplePlayerControlsService"));
    }

    void SimplePlayerControlsSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("SimplePlayerControlsService"));
    }

    void SimplePlayerControlsSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void SimplePlayerControlsSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

	void SimplePlayerControlsSystemComponent::MoveX(float f)
	{
		// If f > 0 move right, else move left
	}

	void SimplePlayerControlsSystemComponent::MoveY(float f)
	{
		// If f > 0 move forward, else move back
		// For now only move forward
		AZ::TickBus::Handler::BusConnect();
	}

	void SimplePlayerControlsSystemComponent::OnTick(float dt, AZ::ScriptTimePoint)
	{
		//AZ_Printf("SPCSC", "%s", "Ticking");
		//AZ::Vector3 dir{ .0f, .0f, .0f };
		//AZ::Vector3 y = AZ::Vector3::CreateAxisY(1.f);
		//AZ::Vector3 n = (dir + y) * 1000.f;
		//AZ::Quaternion q = AZ::Quaternion::CreateIdentity();
		//AZ::TransformBus::EventResult(q, GetEntityId(), &AZ::TransformBus::Events::GetWorldRotationQuaternion);
		//n = q * n;
		//Physics::CharacterRequestBus::Event(GetEntityId(),
		//	&Physics::CharacterRequestBus::Events::TryRelativeMove, n, dt);
		AZ::Vector3 scaledMove = AZ::Vector3::CreateAxisY(1.f) * 100.f;
		AZ::Vector3 newPos = m_lastKnownPosition + scaledMove;
		m_lastKnownPosition = newPos;
		AZ::TransformBus::Event(GetEntityId(), &AZ::TransformBus::Events::SetWorldTranslation, newPos);
	}

    void SimplePlayerControlsSystemComponent::Init()
    {
    }

    void SimplePlayerControlsSystemComponent::Activate()
    {
        SimplePlayerControlsRequestBus::Handler::BusConnect();
		AZ::TransformBus::EventResult(m_lastKnownPosition, GetEntityId(), &AZ::TransformBus::Events::GetWorldTranslation);
    }

    void SimplePlayerControlsSystemComponent::Deactivate()
    {
        SimplePlayerControlsRequestBus::Handler::BusDisconnect();
		AZ::TickBus::Handler::BusDisconnect();
    }
}
