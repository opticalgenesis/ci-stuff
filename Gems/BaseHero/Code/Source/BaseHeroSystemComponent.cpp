#include <BaseHeroSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

#include <AzFramework/Input/Events/InputChannelEventListener.h>
#include <AzFramework/Input/Devices/Mouse/InputDeviceMouse.h>
#include <AzFramework/Input/Devices/Keyboard/InputDeviceKeyboard.h>

#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/ComponentApplicationBus.h>
#include <AzCore/Math/Transform.h>
#include "../../../../../../1.20.0.0/dev/Gems/LmbrCentral/Code/include/LmbrCentral/Physics/CryCharacterPhysicsBus.h"
#include <AzFramework/Network/NetBindingHandlerBus.h>

namespace BaseHero
{
    void BaseHeroSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
			serialize->Class<BaseHeroSystemComponent, AZ::Component>()
				->Version(0)
				->Field("HeroType", &BaseHeroSystemComponent::heroType)
				->Field("HP", &BaseHeroSystemComponent::hp)
				->Field("UltChargeRate", &BaseHeroSystemComponent::ultChargeRatePerMinute)
				->Field("CharacterName", &BaseHeroSystemComponent::charName)
				->Field("MovementScale", &BaseHeroSystemComponent::MovementScale)
				->Field("RotationScale", &BaseHeroSystemComponent::RotationSpeed)
				;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
				ec->Class<BaseHeroSystemComponent>("BaseHero", "[Description of functionality provided by this System Component]")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
					->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
					->Attribute(AZ::Edit::Attributes::AutoExpand, true)
					->DataElement(AZ::Edit::UIHandlers::ComboBox, &BaseHeroSystemComponent::heroType,
						"Hero Type", "Choose from menu")
					->EnumAttribute(BaseHeroSystemComponent::HeroType::HeroDps, "DPS")
					->EnumAttribute(BaseHeroSystemComponent::HeroType::HeroSupport, "Support")
					->EnumAttribute(BaseHeroSystemComponent::HeroType::HeroTank, "Tank")
					->DataElement(AZ::Edit::UIHandlers::Slider, &BaseHeroSystemComponent::hp,
						"HP", "Slide to adjust hero HP")
					->Attribute(AZ::Edit::Attributes::Min, 0.f)
					->Attribute(AZ::Edit::Attributes::Max, 200.f)
					->Attribute(AZ::Edit::Attributes::Step, 5.f)
					->DataElement(AZ::Edit::UIHandlers::Slider, &BaseHeroSystemComponent::ultChargeRatePerMinute,
						"Ult Charge (per minute)", "Adjusts rate Ultimate ability charges per minute")
					->Attribute(AZ::Edit::Attributes::Min, 0.f)
					// subject to change big time
					->Attribute(AZ::Edit::Attributes::Max, 10.f)
					->Attribute(AZ::Edit::Attributes::Step, 0.1f)
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseHeroSystemComponent::charName,
						"Hero name", "Debug prop for hero name")
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseHeroSystemComponent::MovementScale, 
						"Movement speed", "Scalar for Movement speed")
					->DataElement(AZ::Edit::UIHandlers::Default, &BaseHeroSystemComponent::RotationSpeed,
						"Rotation speed", "Scalar for rotation speed")
                    ;
            }
        }
    }

    void BaseHeroSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("BaseHeroService"));
    }

    void BaseHeroSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("BaseHeroService"));
    }

    void BaseHeroSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
		/*required.push_back(AZ_CRC("AsyncService"));*/
		AZ_UNUSED(required);
    }

    void BaseHeroSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void BaseHeroSystemComponent::Init()
    {
		AZ_Printf("Hero", "Created BaseHero with health %f and Ult charge rate of %f", BaseHeroSystemComponent::hp, BaseHeroSystemComponent::ultChargeRatePerMinute);
    }

    void BaseHeroSystemComponent::Activate()
    {
		AZ_Printf("BaseHero", "%s", "Connecting Bus")
        BaseHeroRequestBus::Handler::BusConnect();
		AzFramework::InputChannelEventListener::BusConnect();
		AZ::TickBus::Handler::BusConnect();
		AZ::TransformNotificationBus::Handler::BusConnect(GetEntityId());
		Async::AsyncRequestBus::Handler::BusConnect();

		Async::AsyncRequestBus::Broadcast(&Async::AsyncRequestBus::Events::PrintEbusMessage);
    }

    void BaseHeroSystemComponent::Deactivate()
    {
		AZ_Printf("BaseHero", "%s", "Disconnecting Bus")
        BaseHeroRequestBus::Handler::BusDisconnect();
		AzFramework::InputChannelEventListener::BusDisconnect();
		AZ::TickBus::Handler::BusDisconnect();
		AZ::TransformNotificationBus::Handler::BusDisconnect();
		Async::AsyncRequestBus::Handler::BusDisconnect();
    }

	void BaseHeroSystemComponent::SetHp(float hp)
	{
		BaseHeroSystemComponent::hp = hp;
	}

	bool BaseHeroSystemComponent::OnInputChannelEventFiltered(const AzFramework::InputChannel &input)
	{
		AZ_Printf("BaseHero", "%s", "Filtering input event")
		auto device_id = input.GetInputDevice().GetInputDeviceId();

		if (device_id == AzFramework::InputDeviceMouse::Id)
		{
			OnMouseEvent(input);
		}
		else if (device_id == AzFramework::InputDeviceKeyboard::Id)
		{
			OnKeyboardEvent(input);
		}
		return false;
	}

	void BaseHeroSystemComponent::OnKeyboardEvent(const AzFramework::InputChannel &input)
	{
		auto input_type = input.GetInputChannelId();
		AZ_Printf("Hero", "%s", "Keyboard event");
		if (input_type == AzFramework::InputDeviceKeyboard::Key::AlphanumericW)
		{
			bIsMovingForward = !!input.GetValue();
		}
		else if (input_type == AzFramework::InputDeviceKeyboard::Key::AlphanumericS)
		{
			bIsMovingBackward = !!input.GetValue();
		}
		else if (input_type == AzFramework::InputDeviceKeyboard::Key::AlphanumericA)
		{
			bIsStrafingLeft = !!input.GetValue();
		}
		else if (input_type == AzFramework::InputDeviceKeyboard::Key::AlphanumericD)
		{
			bIsStrafingRight = !!input.GetValue();
		}
		else if (input_type == AzFramework::InputDeviceKeyboard::Key::EditSpace)
		{
			bIsJumping = !!input.GetValue();
		}
	}

	void BaseHeroSystemComponent::OnMouseEvent(const AzFramework::InputChannel &input) 
	{
		AZ_Printf("Hero", "%s", "Mouse event");
		auto input_type = input.GetInputChannelId();
		if (input_type == AzFramework::InputDeviceMouse::SystemCursorPosition)
		{
			PerformRotation(input);
		}
	}

	void BaseHeroSystemComponent::CenterCursorPosition()
	{
		EBUS_EVENT(AzFramework::InputSystemCursorRequestBus,
			SetSystemCursorPositionNormalized, AZ::Vector2{ .5f, .5f });
		m_lastMousePosition = AZ::Vector2{ .5f, .5f };
	}

	void BaseHeroSystemComponent::PerformRotation(const AzFramework::InputChannel &input)
	{
		auto pos_data = input.GetCustomData<AzFramework::InputChannel::PositionData2D>();
		TrackMouseMovement(pos_data);
		CenterCursorPosition();
	}

	void BaseHeroSystemComponent::TrackMouseMovement(const AzFramework::InputChannel::PositionData2D *pos_data)
	{
		auto deltaMousePos = m_lastMousePosition - pos_data->m_normalizedPosition;
		m_lastMousePosition = pos_data->m_normalizedPosition;
		m_mouseChangeAggregate += deltaMousePos;
	}

	void BaseHeroSystemComponent::OnTick(float dT, AZ::ScriptTimePoint time)
	{
		AZ::Transform entityTransform;
		EBUS_EVENT_ID_RESULT(entityTransform, GetEntityId(), AZ::TransformBus, GetWorldTM);

		entityTransform.SetRotationPartFromQuaternion(GetCurrentOrientation());
		EBUS_EVENT_ID(GetEntityId(), AZ::TransformBus, SetWorldTM, entityTransform);

		auto rotation = GetCurrentOrientation();
		entityTransform.SetRotationPartFromQuaternion(rotation);
		EBUS_EVENT_ID(GetEntityId(), AZ::TransformBus, SetWorldTM, entityTransform);

		auto desiredVelocity = AZ::Vector3::CreateZero();

		if (bIsMovingForward || bIsMovingBackward
			|| bIsStrafingLeft || bIsStrafingRight
			|| bIsJumping)
		{
			HandleYAxis(desiredVelocity);
			HandleXAxis(desiredVelocity);
			HandleZAxis(desiredVelocity);

			desiredVelocity = rotation * desiredVelocity;
		}

		EBUS_EVENT_ID(GetEntityId(), LmbrCentral::CryCharacterPhysicsRequestBus, RequestVelocity, desiredVelocity, 0);
	}

	const AZ::Quaternion BaseHeroSystemComponent::GetCurrentOrientation()
	{
		auto z_rotation = AZ::Quaternion::CreateRotationZ(m_mouseChangeAggregate.GetX() * RotationSpeed);
		return z_rotation;
	}

	void BaseHeroSystemComponent::HandleYAxis(AZ::Vector3 &desiredVelocity)
	{
		if (bIsMovingForward || bIsMovingBackward)
		{
			float forward_back_velocity = 0;

			if (bIsMovingForward)
			{
				forward_back_velocity += MovementScale;
			}
			else
			{
				forward_back_velocity -= (MovementScale / 2.f);
			}

			desiredVelocity.SetY(forward_back_velocity);
		}
	}

	void BaseHeroSystemComponent::HandleXAxis(AZ::Vector3 &desiredVelocity)
	{
		if (bIsStrafingLeft || bIsStrafingRight)
		{
			float left_right_velocity = 0;

			if (bIsStrafingRight)
			{
				left_right_velocity += MovementScale * .75f;
			}
			else
			{
				left_right_velocity -= MovementScale * .75f;
			}

			desiredVelocity.SetX(left_right_velocity);
		}
	}

	void BaseHeroSystemComponent::HandleZAxis(AZ::Vector3 &desiredVelocity)
	{
		if (bIsJumping)
		{
			desiredVelocity.SetZ(JumpHeight);
		}
	}
}