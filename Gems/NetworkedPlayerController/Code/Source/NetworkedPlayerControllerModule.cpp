
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <NetworkedPlayerControllerSystemComponent.h>

namespace NetworkedPlayerController
{
    class NetworkedPlayerControllerModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(NetworkedPlayerControllerModule, "{ED49FB89-1C14-4623-BC19-2A8E1CF9554C}", AZ::Module);
        AZ_CLASS_ALLOCATOR(NetworkedPlayerControllerModule, AZ::SystemAllocator, 0);

        NetworkedPlayerControllerModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                NetworkedPlayerControllerSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<NetworkedPlayerControllerSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(NetworkedPlayerController_2f9f98591e9a4038bac12b90e37ec502, NetworkedPlayerController::NetworkedPlayerControllerModule)
