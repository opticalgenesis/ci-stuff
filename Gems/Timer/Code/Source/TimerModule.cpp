
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TimerSystemComponent.h>

namespace Timer
{
    class TimerModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TimerModule, "{DADD742D-7C5A-440C-BE57-832740F21116}", AZ::Module);
        AZ_CLASS_ALLOCATOR(TimerModule, AZ::SystemAllocator, 0);

        TimerModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TimerSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<TimerSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(Timer_78f7da6607df443283e6d7a68e14f608, Timer::TimerModule)
