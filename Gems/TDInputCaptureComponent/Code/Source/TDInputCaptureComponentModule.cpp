
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <TDInputCaptureComponentSystemComponent.h>

namespace TDInputCaptureComponent
{
    class TDInputCaptureComponentModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(TDInputCaptureComponentModule, "{5F2A5FEE-7F41-4012-BDA5-049D46F089C5}", AZ::Module);
        AZ_CLASS_ALLOCATOR(TDInputCaptureComponentModule, AZ::SystemAllocator, 0);

        TDInputCaptureComponentModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                TDInputCaptureComponentSystemComponent::CreateDescriptor(),
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(TDInputCaptureComponent_0ea36030feab41f28bf8be7427a5c707, TDInputCaptureComponent::TDInputCaptureComponentModule)
