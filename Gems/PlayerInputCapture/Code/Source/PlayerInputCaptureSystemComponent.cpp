
#include <PlayerInputCaptureSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace PlayerInputCapture
{
    void PlayerInputCaptureSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<PlayerInputCaptureSystemComponent, AZ::Component>()
                ->Version(0);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<PlayerInputCaptureSystemComponent>("PlayerInputCapture", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void PlayerInputCaptureSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("PlayerInputCaptureService"));
    }

    void PlayerInputCaptureSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("PlayerInputCaptureService"));
    }

    void PlayerInputCaptureSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
        AZ_UNUSED(required);
    }

    void PlayerInputCaptureSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void PlayerInputCaptureSystemComponent::Init()
    {
    }

    void PlayerInputCaptureSystemComponent::Activate()
    {
        PlayerInputCaptureRequestBus::Handler::BusConnect();
    }

    void PlayerInputCaptureSystemComponent::Deactivate()
    {
        PlayerInputCaptureRequestBus::Handler::BusDisconnect();
    }
}
