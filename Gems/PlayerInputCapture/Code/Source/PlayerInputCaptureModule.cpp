
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include <PlayerInputCaptureSystemComponent.h>

namespace PlayerInputCapture
{
    class PlayerInputCaptureModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(PlayerInputCaptureModule, "{B05A1FA3-D9E7-4C4D-AC4A-0D42B96FC2DE}", AZ::Module);
        AZ_CLASS_ALLOCATOR(PlayerInputCaptureModule, AZ::SystemAllocator, 0);

        PlayerInputCaptureModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                PlayerInputCaptureSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<PlayerInputCaptureSystemComponent>(),
            };
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(PlayerInputCapture_48d11abcc6804c339abb5c842063a9ee, PlayerInputCapture::PlayerInputCaptureModule)
