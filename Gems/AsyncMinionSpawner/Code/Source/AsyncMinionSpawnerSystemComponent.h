#pragma once

#include <AzCore/Component/Component.h>

#include <AzFramework/Input/Buses/Notifications/InputChannelNotificationBus.h>
#include <AzFramework/Network/NetBindingHandlerBus.h>

#include <LmbrCentral/Scripting/SpawnerComponentBus.h>

#include <AsyncMinionSpawner/AsyncMinionSpawnerBus.h>

namespace AsyncMinionSpawner
{
    class AsyncMinionSpawnerSystemComponent
        : public AZ::Component
        , protected AsyncMinionSpawnerRequestBus::Handler
    {
    public:
        AZ_COMPONENT(AsyncMinionSpawnerSystemComponent, "{722CA3B9-9BB2-4F16-882D-A3D4C6C4C12F}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

    protected:
        ////////////////////////////////////////////////////////////////////////
        // AsyncMinionSpawnerRequestBus interface implementation
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
		
		int m_spawnCount;
		// Don't use these, rotate the spawner to control the initial direction
		AZ::Vector3 m_InitialVelocity;
		AZ::Vector3 m_InitialDirection;

	private:
		void Spawn();
		AZStd::thread m_spawningThread;
    };
}
