#pragma once

#include <AzCore/Component/Component.h>

#include <AsyncMinionSpawner/AsyncSpawnerFunnelBus.h>

namespace AsyncMinionSpawner
{
	class AsyncFunnel
		: public AZ::Component
	{
	public:
		AZ_COMPONENT(AsyncFunnel, "{78A8C280-4EA5-405A-97A3-8085D84C771E}");

		void Init() override;
		void Activate() override;
		void Deactivate() override;

		static void Reflect(AZ::ReflectContext*);
	};
}