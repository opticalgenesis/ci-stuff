
#include <AsyncMinionSpawnerSystemComponent.h>

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

namespace AsyncMinionSpawner
{
    void AsyncMinionSpawnerSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
			serialize->Class<AsyncMinionSpawnerSystemComponent, AZ::Component>()
				->Version(0)
				->Field("SpawnCount", &AsyncMinionSpawnerSystemComponent::m_spawnCount)
				->Field("InitialVelocity", &AsyncMinionSpawnerSystemComponent::m_InitialVelocity)
				->Field("InitialDirection", &AsyncMinionSpawnerSystemComponent::m_InitialDirection)
				;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<AsyncMinionSpawnerSystemComponent>("AsyncMinionSpawner", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
						->Attribute(AZ::Edit::Attributes::Category, "TowerDefence")
					->DataElement(nullptr, &AsyncMinionSpawnerSystemComponent::m_spawnCount, "Spawn Count", "The number of minions this entity should spawn")
					->DataElement(AZ::Edit::UIHandlers::Vector3, &AsyncMinionSpawnerSystemComponent::m_InitialVelocity,
						"Initial Velocity", "The inital velocity to impart on the minion")
					->DataElement(AZ::Edit::UIHandlers::Vector3, &AsyncMinionSpawnerSystemComponent::m_InitialDirection,
						"Initial Direction", "The Direction to send the minion in")
                    ;
            }
        }
    }

    void AsyncMinionSpawnerSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("AsyncMinionSpawnerService"));
    }

    void AsyncMinionSpawnerSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("AsyncMinionSpawnerService"));
    }

    void AsyncMinionSpawnerSystemComponent::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
    {
		AZ_UNUSED(required);
    }

    void AsyncMinionSpawnerSystemComponent::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
        AZ_UNUSED(dependent);
    }

    void AsyncMinionSpawnerSystemComponent::Init()
    {
    }

    void AsyncMinionSpawnerSystemComponent::Activate()
    {
        AsyncMinionSpawnerRequestBus::Handler::BusConnect(GetEntityId());
		// Make the server authoritative on the spawned entities
		if (AzFramework::NetQuery::IsEntityAuthoritative(GetEntityId()))
		{
			// Descriptor for function to be ran asynchronously
			AZStd::function<void()> spawnFunc = AZStd::bind(&AsyncMinionSpawnerSystemComponent::Spawn, this);
			AZStd::thread_desc spawnDesc;
			// Probably unnecessary but console complains if not explicitly set
			spawnDesc.m_isJoinable = true;
			// Spawns the new thread and runs the spawn function
			m_spawningThread = AZStd::thread(spawnFunc, &spawnDesc);
		}
    }

    void AsyncMinionSpawnerSystemComponent::Deactivate()
    {
        AsyncMinionSpawnerRequestBus::Handler::BusDisconnect();
		m_spawningThread.detach();
    }

	void AsyncMinionSpawnerSystemComponent::Spawn()
	{
		for (int i = 0; i < m_spawnCount; i++)
		{
			// Set Mutex type on the Bus in core engine code to suppress warnings about calls from multiple threads
			// Complaint coming from another Bus
			LmbrCentral::SpawnerComponentRequestBus::Event(GetEntityId(),
				&LmbrCentral::SpawnerComponentRequestBus::Events::Spawn);
			// Sleep the thread to allow the previously spawned minion to move out of the way of the new one
			AZStd::this_thread::sleep_for(AZStd::chrono::seconds(3));
		}
		if (m_spawningThread.joinable()) m_spawningThread.join();
	}
}
