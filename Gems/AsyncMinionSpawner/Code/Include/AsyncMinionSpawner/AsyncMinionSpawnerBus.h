#pragma once

#include <AzCore/EBus/EBus.h>

namespace AsyncMinionSpawner
{
    class AsyncMinionSpawnerRequests
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static const AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static const AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::ById;
        //////////////////////////////////////////////////////////////////////////
		using BusIdType = AZ::EntityId;
        // Put your public methods here
    };
    using AsyncMinionSpawnerRequestBus = AZ::EBus<AsyncMinionSpawnerRequests>;
} // namespace AsyncMinionSpawner
